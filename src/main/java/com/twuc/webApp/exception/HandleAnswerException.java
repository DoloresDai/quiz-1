package com.twuc.webApp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandleAnswerException {

    @ExceptionHandler(AnswerException.class)
    public ResponseEntity<String> handleAnswerException() {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .build();
    }
}
