package com.twuc.webApp.controller;

import com.twuc.webApp.model.Game;
import com.twuc.webApp.model.GameId;
import com.twuc.webApp.model.Hint;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.net.URL;


@RestController
@RequestMapping("/api/games")
public class GameController {
    @GetMapping("/{id}")
    public Game getAnswer(@PathVariable("id") int id) {
        Game game = new Game();
        game.setId(id);
        game.setAnswer("1234");
        return game;
    }

    @PatchMapping("/{id}")
    public Hint getHint(@PathVariable("id") int id, @RequestParam("gameAnswer") String gameAnswer) {
        Game game = new Game();
        game.setId(id);
        game.setAnswer("1234");
        game.setGamerAnswer(gameAnswer);
        return game.getHint();
    }

    @PostMapping("")
    public ResponseEntity<String> createNewGame(UriComponentsBuilder ucb){
        /*        String gameId = new GameId().getGameId();*/
        URI url = ucb.path("/api/games/")
                .path("9864")
                .build()
                .toUri();
        return ResponseEntity
                .created(url)
                .build();
    }
}
