package com.twuc.webApp.model;

public class Hint {
    private String hint;
    private boolean correct;

    public Hint(String hint) {
        this.hint = hint;
    }

    public Hint() {
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }
}
