package com.twuc.webApp.model;

import java.util.Random;

public class GameId {
    public String getGameId() {
        return gameId;
    }

    private String gameId;

    public GameId() {
        setGameId();
    }

    public void setGameId() {
        String str = "0123456789";
        StringBuilder sb = new StringBuilder(4);
        for (int i = 0; i < 4; i++) {
            char ch = str.charAt(new Random().nextInt(str.length()));
            sb.append(ch);
        }
        this.gameId = sb.toString();
    }
}
