package com.twuc.webApp.model;

import com.twuc.webApp.exception.AnswerException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {

    private String answer;
    private Integer id;
    private Hint hint = new Hint();
    private String gamerAnswer;


    public String getGamerAnswer() {
        return gamerAnswer;
    }

    public void setGamerAnswer(String gamerAnswer) {
        if (gamerAnswer.length() != 4) {
            throw new AnswerException();
        }
        Pattern pattern = Pattern.compile("^(?!\\d*?(\\d)\\d*?\\1)\\d{4}$");
        Matcher matcher = pattern.matcher(gamerAnswer);
        if (matcher.find()) {
            if (matcher.group().equals(gamerAnswer)) {
                this.gamerAnswer = gamerAnswer;
            }
        }
        throw new AnswerException();
    }

    public void setHint(Hint hint) {
        this.hint = hint;
    }

    public Game() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public Hint getHint() {

        if ((this.gamerAnswer.equals(this.answer))) {
            this.hint.setCorrect(true);
            this.hint.setHint("4A0B");
            return this.hint;
        }
        String[] splitAnswer = answer.split("");
        String[] splitGamerAnswer = gamerAnswer.split("");
        int first = 0, second = 0;
        for (int i = 0; i < splitAnswer.length; i++) {
            if (splitAnswer[i].equals(splitGamerAnswer[i])) {
                first++;
            }
            if (answer.toString().contains(splitGamerAnswer[i])) {
                second++;
            }
        }
        second = second - first;
        String hint = String.join("", String.valueOf(first), "A", String.valueOf(second), "B");
        this.hint.setHint(hint);
        return this.hint;
    }

}
