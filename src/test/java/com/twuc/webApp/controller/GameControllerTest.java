package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.stream.Location;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Test
    void should_get_correct_answer_when_get_gameId() throws Exception {
        mockMvc.perform(get("/api/games/2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id").value("2"))
                .andExpect(jsonPath("$.answer").value("1234"));
    }

    @Test
    void should_get_Hint_when_PATCH() throws Exception {
        mockMvc.perform(patch("/api/games/2")
                .param("gameAnswer", "1278"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.hint").value("2A0B"))
                .andExpect(jsonPath("$.correct").value(false));

        mockMvc.perform(patch("/api/games/2")
                .param("gameAnswer", "1234"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.hint").value("4A0B"))
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_return_404_when_gamerAnswer_length_is_not_4() throws Exception {
        mockMvc.perform(patch("/api/games/3")
                .param("gameAnswer", "127"))
                .andExpect(status().isNotFound());

        mockMvc.perform(patch("/api/games/3")
                .param("gameAnswer", "12790"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_404_when_gamerAnswer_contains_letter() throws Exception {
        mockMvc.perform(patch("/api/games/3")
                .param("gameAnswer", "127a"))
                .andExpect(status().isNotFound());

        mockMvc.perform(patch("/api/games/3")
                .param("gameAnswer", "12790i"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_create_a_game_when_post_by_games() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location","http://localhost/api/games/9864"));
    }
}
